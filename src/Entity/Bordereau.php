<?php

namespace App\Entity;

use App\Repository\BordereauRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BordereauRepository::class)
 */
class Bordereau
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_enterprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address_enterprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postal_code_enterprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $principal_activity_enterprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $SIRET;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_accountant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $function_accountant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone_accountant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail_accountant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_1_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_2_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_3_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_1_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_2_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_3_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_1_amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_2_amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $institution_3_amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ladapt_amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $total_amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $payment_methode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameEnterprise(): ?string
    {
        return $this->name_enterprise;
    }

    public function setNameEnterprise(string $name_enterprise): self
    {
        $this->name_enterprise = $name_enterprise;

        return $this;
    }

    public function getAddressEnterprise(): ?string
    {
        return $this->address_enterprise;
    }

    public function setAddressEnterprise(string $address_enterprise): self
    {
        $this->address_enterprise = $address_enterprise;

        return $this;
    }

    public function getPostalCodeEnterprise(): ?string
    {
        return $this->postal_code_enterprise;
    }

    public function setPostalCodeEnterprise(string $postal_code_enterprise): self
    {
        $this->postal_code_enterprise = $postal_code_enterprise;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPrincipalActivityEnterprise(): ?string
    {
        return $this->principal_activity_enterprise;
    }

    public function setPrincipalActivityEnterprise(string $principal_activity_enterprise): self
    {
        $this->principal_activity_enterprise = $principal_activity_enterprise;

        return $this;
    }

    public function getSIRET(): ?string
    {
        return $this->SIRET;
    }

    public function setSIRET(string $SIRET): self
    {
        $this->SIRET = $SIRET;

        return $this;
    }

    public function getNameAccountant(): ?string
    {
        return $this->name_accountant;
    }

    public function setNameAccountant(string $name_accountant): self
    {
        $this->name_accountant = $name_accountant;

        return $this;
    }

    public function getFunctionAccountant(): ?string
    {
        return $this->function_accountant;
    }

    public function setFunctionAccountant(string $function_accountant): self
    {
        $this->function_accountant = $function_accountant;

        return $this;
    }

    public function getPhoneAccountant(): ?string
    {
        return $this->phone_accountant;
    }

    public function setPhoneAccountant(string $phone_accountant): self
    {
        $this->phone_accountant = $phone_accountant;

        return $this;
    }

    public function getMailAccountant(): ?string
    {
        return $this->mail_accountant;
    }

    public function setMailAccountant(string $mail_accountant): self
    {
        $this->mail_accountant = $mail_accountant;

        return $this;
    }

    public function getInstitution1Name(): ?string
    {
        return $this->institution_1_name;
    }

    public function setInstitution1Name(string $institution_1_name): self
    {
        $this->institution_1_name = $institution_1_name;

        return $this;
    }

    public function getInstitution2Name(): ?string
    {
        return $this->institution_2_name;
    }

    public function setInstitution2Name(string $institution_2_name): self
    {
        $this->institution_2_name = $institution_2_name;

        return $this;
    }

    public function getInstitution3Name(): ?string
    {
        return $this->institution_3_name;
    }

    public function setInstitution3Name(string $institution_3_name): self
    {
        $this->institution_3_name = $institution_3_name;

        return $this;
    }

    public function getInstitution1Code(): ?string
    {
        return $this->institution_1_code;
    }

    public function setInstitution1Code(string $institution_1_code): self
    {
        $this->institution_1_code = $institution_1_code;

        return $this;
    }

    public function getInstitution2Code(): ?string
    {
        return $this->institution_2_code;
    }

    public function setInstitution2Code(string $institution_2_code): self
    {
        $this->institution_2_code = $institution_2_code;

        return $this;
    }

    public function getInstitution3Code(): ?string
    {
        return $this->institution_3_code;
    }

    public function setInstitution3Code(string $institution_3_code): self
    {
        $this->institution_3_code = $institution_3_code;

        return $this;
    }

    public function getInstitution1Amount(): ?string
    {
        return $this->institution_1_amount;
    }

    public function setInstitution1Amount(string $institution_1_amount): self
    {
        $this->institution_1_amount = $institution_1_amount;

        return $this;
    }

    public function getInstitution2Amount(): ?string
    {
        return $this->institution_2_amount;
    }

    public function setInstitution2Amount(string $institution_2_amount): self
    {
        $this->institution_2_amount = $institution_2_amount;

        return $this;
    }

    public function getInstitution3Amount(): ?string
    {
        return $this->institution_3_amount;
    }

    public function setInstitution3Amount(string $institution_3_amount): self
    {
        $this->institution_3_amount = $institution_3_amount;

        return $this;
    }

    public function getLadaptAmount(): ?string
    {
        return $this->ladapt_amount;
    }

    public function setLadaptAmount(string $ladapt_amount): self
    {
        $this->ladapt_amount = $ladapt_amount;

        return $this;
    }

    public function getTotalAmount(): ?string
    {
        return $this->total_amount;
    }

    public function setTotalAmount(string $total_amount): self
    {
        $this->total_amount = $total_amount;

        return $this;
    }

    public function getPaymentMethode(): ?string
    {
        return $this->payment_methode;
    }

    public function setPaymentMethode(string $payment_methode): self
    {
        $this->payment_methode = $payment_methode;

        return $this;
    }
}
