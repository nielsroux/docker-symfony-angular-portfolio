import { createApp } from "vue";
import Footer from "./components/Footer.vue";

createApp(Footer).mount("#footer");
